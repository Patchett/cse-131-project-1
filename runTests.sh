#!/bin/bash

errorOccurred=0

rm -rf myOutputs
mkdir myOutputs
make clean
make
cd samples/Tests

for filename in *.frag; do
  `../../glc < ${filename%.*}.frag &> ../../myOutputs/${filename%.*}.out`
  success=$(diff -q ../Outputs/${filename%.*}.out ../../myOutputs/${filename%.*}.out)
  # $? is stores the exit value of the last command that was executed
  if [ $? -eq 0 ]; then
    echo -e "\nSUCCESS: Test for ${filename%.*}.frag passed."
  else
    echo -e "\nERROR: Test for ${filename%.*}.frag failed."
    errorOccurred=1
  fi
done

cd ../../
