/* File:  scanner.l
 * ----------------
 * Lex input file to generate the scanner for the compiler.
 */

%{

/* The text within this first region delimited by %{ and %} is assumed to
 * be C/C++ code and will be copied verbatim to the lex.yy.c file ahead
 * of the definitions of the yylex() function. Add other header file inclusions
 * or C++ variable declarations/prototypes that are needed by your code here.
 */

#include <string.h>
#include "scanner.h"
#include "utility.h" // for PrintDebug()
#include "errors.h"

/* Global variable: yylval
 * -----------------------
 * This global variable is how we get attribute information about the token
 * just scanned to the client. The scanner sets the global variable
 * appropriately and since it's global the client can just read it.  In the
 * future, this variable will be declared for us in the y.tab.c file
 * produced by Yacc, but for now, we declare it manually.
 */
YYSTYPE yylval;  // manually declared for pp1, later Yacc provides

/* Global variable: yylloc
 * -----------------------
 * This global variable is how we get position information about the token
 * just scanned to the client. (Operates similarly to yylval above)
 */
struct yyltype yylloc; // manually dclared for pp1, later Yacc provides

/* Macro: YY_USER_ACTION
 * ---------------------
 * This flex built-in macro can be defined to provide an action which is
 * always executed prior to any matched rule's action. Basically, it is
 * a way of having a piece of code common to all actions factored out to
 * this routine.  We already defined it for you and left the empty
 * function DoBeforeEachAction ready for your use as needed. It will
 * be called once for each pattern scanned from the file, before
 * executing its action.
 */
static void DoBeforeEachAction();
static int gTokenAction(int token);
static int constantAction(int token);
static void parseComment();
static char operatorAction();
static int identifierAction(int token);

#define YY_USER_ACTION DoBeforeEachAction();

%}

 /* The section before the first %% is the Definitions section of the lex
  * input file. Here is where you set options for the scanner, define lex
  * states, and can set up definitions to give names to regular expressions
  * as a simple substitution mechanism that allows for more readable
  * entries in the Rules section later.
  */

%x C_COMMENTS

%%             /* BEGIN RULES SECTION */
 /* All patterns and actions should be placed between the start and stop
  * %% markers which delimit the Rules section.
  */

"/*" {BEGIN(C_COMMENTS);}
<C_COMMENTS>"/*"
<C_COMMENTS>[^*\n]*
<C_COMMENTS>"*"+[^*/\n]*
<C_COMMENTS>\n { yylloc.first_line++; yylloc.last_column = 0; yylloc.first_column = 1; }
<C_COMMENTS><<EOF>> {ReportError::UntermComment(); return 0;}
<C_COMMENTS>"*"+"/" {BEGIN(INITIAL);}

"//"[^\n]* {}
[\t] {yylloc.last_column+=7; yylloc.first_column+=7;}
[ ] {;}
[\n]|[\r\n]|[\r] {yylloc.first_line++; yylloc.last_column = 0; yylloc.first_column = 1;}
void {TokenType t = T_Void; gTokenAction(t);}
int {TokenType t = T_Int; gTokenAction(t);}
float {TokenType t = T_Float; gTokenAction(t);}
bool {TokenType t = T_Bool; gTokenAction(t);}
while {TokenType t = T_While; gTokenAction(t);}
for {TokenType t = T_For; gTokenAction(t);}
if {TokenType t = T_If; gTokenAction(t);}
else {TokenType t = T_Else; gTokenAction(t);}
return {TokenType t = T_Return; gTokenAction(t);}
break {TokenType t = T_Break; gTokenAction(t);}
true|false {TokenType t = T_BoolConstant; constantAction(t);}
const {TokenType t = T_Const; gTokenAction(t);}
uniform {TokenType t = T_Uniform; gTokenAction(t);}
layout {TokenType t = T_Layout; gTokenAction(t);}
continue {TokenType t = T_Continue; gTokenAction(t);}
do {TokenType t = T_Do; gTokenAction(t);}
switch {TokenType t = T_Switch; gTokenAction(t);}
case {TokenType t = T_Case; gTokenAction(t);}
default {TokenType t = T_Default; gTokenAction(t);}
in {TokenType t = T_In; gTokenAction(t);}
out {TokenType t = T_Out; gTokenAction(t);}
inout {TokenType t = T_Inout; gTokenAction(t);}
mat2 {TokenType t = T_Mat2; gTokenAction(t);}
mat3 {TokenType t = T_Mat3; gTokenAction(t);}
mat4 {TokenType t = T_Mat4; gTokenAction(t);}
vec2 {TokenType t = T_Vec2; gTokenAction(t);}
vec3 {TokenType t = T_Vec3; gTokenAction(t);}
vec4 {TokenType t = T_Vec4; gTokenAction(t);}
ivec2 {TokenType t = T_Ivec2; gTokenAction(t);}
ivec3 {TokenType t = T_Ivec3; gTokenAction(t);}
ivec4 {TokenType t = T_Ivec4; gTokenAction(t);}
bvec2 {TokenType t = T_Bvec2; gTokenAction(t);}
bvec3 {TokenType t = T_Bvec3; gTokenAction(t);}
bvec4 {TokenType t = T_Bvec4; gTokenAction(t);}
uint {TokenType t = T_Uint; gTokenAction(t);}
uvec2 {TokenType t = T_Uvec2; gTokenAction(t);}
uvec3 {TokenType t = T_Uvec3; gTokenAction(t);}
uvec4 {TokenType t = T_Uvec4; gTokenAction(t);}
struct {TokenType t = T_Struct; gTokenAction(t);}
NULL {TokenType t = T_NULL; gTokenAction(t);}
([0-9]+\.[0-9]*f?)|([0-9]*\.[0-9]+f?)|([0-9]+f) {TokenType t = T_FloatConstant; constantAction(t);}
[0-9]+ {TokenType t = T_IntConstant; constantAction(t);}
"<=" {TokenType t = T_LessEqual; gTokenAction(t);}
">=" {TokenType t = T_GreaterEqual; gTokenAction(t);}
"==" {TokenType t = T_Equal; gTokenAction(t);}
"!=" {TokenType t = T_NotEqual; gTokenAction(t);}
"&&" {TokenType t = T_And; gTokenAction(t);}
"||" {TokenType t = T_Or; gTokenAction(t);}
"++" {TokenType t = T_Inc; gTokenAction(t);}
"--" {TokenType t = T_Dec; gTokenAction(t);}
\[\] {TokenType t = T_Dims; gTokenAction(t);}
\" {operatorAction();}
"+" {operatorAction();}
"*" {operatorAction();}
"/" {operatorAction();}
"%" {operatorAction();}
"<" {operatorAction();}
">" {operatorAction();}
"=" {operatorAction();}
"-" {operatorAction();}
"!" {operatorAction();}
";" {operatorAction();}
"," {operatorAction();}
"." {operatorAction();}
"[" {operatorAction();}
"]" {operatorAction();}
"(" {operatorAction();}
")" {operatorAction();}
"{" {operatorAction();}
"}" {operatorAction();}
"?" {operatorAction();}
"#" {operatorAction();}
"^" {operatorAction();}
"|" {operatorAction();}
\\  {operatorAction();}
"&" {operatorAction();}
"~" {operatorAction();}
":" {operatorAction();}
[_]*[a-zA-Z][A-Za-z0-9_]* {TokenType t = T_Identifier; identifierAction(t);}
[_]+[a-zA-Z0-9][A-Za-z0-9_]* {TokenType t = T_Identifier; identifierAction(t);}
. {ReportError::UnrecogChar(&yylloc, yytext[0]);}

%%
/* The closing %% above marks the end of the Rules section and the beginning
 * of the User Subroutines section. All text from here to the end of the
 * file is copied verbatim to the end of the generated lex.yy.c file.
 * This section is where you put definitions of helper functions.
 */

static int identifierAction(int token)
{
  if (strlen(yytext) > 1023)
     ReportError::LongIdentifier(&yylloc, yytext);

  else if (strlen(yytext) > MaxIdentLen )
  {
    strncpy(yylval.identifier, yytext, MaxIdentLen );
    printf("%-12s line %d cols %d-%d is %s (truncated to %s)\n", yytext, yylloc.first_line,
        yylloc.first_column, yylloc.last_column, gTokenNames[token - T_Void], yylval.identifier);
  }
  else
  {
    strncpy(yylval.identifier, yytext, MaxIdentLen);
    printf("%-12s line %d cols %d-%d is %s \n", yylval.identifier, yylloc.first_line,
      yylloc.first_column, yylloc.last_column, gTokenNames[token - T_Void]);

  }
   return token;
}

static char operatorAction()
{
  char buffer[] = {'\'', yytext[0], '\'', '\0'};
  //PrintOneToken(token, yytext, yylval, yylloc);
  printf("%-12s line %d cols %d-%d is %s \n", yytext, yylloc.first_line,
      yylloc.first_column, yylloc.last_column, buffer);

  return yytext[0];
}

static int gTokenAction(int token)
{
    /*token is an integer code returned by the parser greater than 256*/
    // yylloc.last_column += yyleng;
    printf("%-12s line %d cols %d-%d is %s \n", yytext, yylloc.first_line,
    yylloc.first_column, yylloc.last_column, gTokenNames[token - T_Void]);
    // yylloc.first_column += yyleng;
    return token;
}

static void parseComment() {
  for (int i = 0; i < strlen(yytext); i++) {
     if (yytext[i] == '\n') {
       yylloc.first_line++; yylloc.last_column = 0; yylloc.first_column = 1;
     }
   }
}

static int constantAction(int token) {
    /*token is an integer code returned by the parser greater than 256*/
    // yylloc.last_column += yyleng;
    if(strcmp("T_IntConstant", gTokenNames[token - T_Void]) == 0) {
        yylval.integerConstant = strtol(yytext, NULL, 0);
        printf("%-12s line %d cols %d-%d is %s (value = %d)\n", yytext, yylloc.first_line,
        yylloc.first_column, yylloc.last_column, gTokenNames[token - T_Void], yylval.integerConstant);
    } else if (strcmp("T_BoolConstant", gTokenNames[token - T_Void]) == 0) {
        if(strcmp(yytext, "true") == 0) {
            yylval.boolConstant = true;
            printf("%-12s line %d cols %d-%d is %s (value = %s)\n", yytext, yylloc.first_line,
            yylloc.first_column, yylloc.last_column, gTokenNames[token - T_Void], yytext);
        } else if (strcmp(yytext, "false") == 0) {
            yylval.boolConstant = false;
            printf("%-12s line %d cols %d-%d is %s (value = %s)\n", yytext, yylloc.first_line,
            yylloc.first_column, yylloc.last_column, gTokenNames[token - T_Void], yytext);
        }
    } else if (strcmp("T_FloatConstant", gTokenNames[token - T_Void]) == 0) {
        yylval.floatConstant = atof(yytext);
        printf("%-12s line %d cols %d-%d is %s (value = %g)\n", yytext, yylloc.first_line,
        yylloc.first_column, yylloc.last_column, gTokenNames[token - T_Void], yylval.floatConstant);
    }
    // yylloc.first_column += yyleng;
    return token;
}


/* Function: InitScanner
 * ---------------------
 * This function will be called before any calls to yylex().  It is designed
 * to give you an opportunity to do anything that must be done to initialize
 * the scanner (set global variables, configure starting state, etc.). One
 * thing it already does for you is assign the value of the global variable
 * yy_flex_debug that controls whether flex prints debugging information
 * about each token and what rule was matched. If set to false, no information
 * is printed. Setting it to true will give you a running trail that might
 * be helpful when debugging your scanner. Please be sure the variable is
 * set to false when submitting your final version.
 */
void InitScanner()
{
    PrintDebug("lex", "Initializing scanner");
    yy_flex_debug = false;

    yylloc.first_line = 1;
    yylloc.last_column = 0;
    yylloc.first_column = 1;
}

/* Function: DoBeforeEachAction()
 * ------------------------------
 * This function is installed as the YY_USER_ACTION. This is a place
 * to group code common to all actions.
 */

static void DoBeforeEachAction()
{
    yylloc.first_column = yylloc.last_column + 1;
    yylloc.last_column = yylloc.first_column + yyleng - 1;
}


/* TODO */
/*What happens with nested comments?! */

/* put in ++ and -- DONE*/
/* add GLSL identifier rules DONE */
/* return an error for incomplete comments DONE */
/* truncate inputs over 1023 DONE */
/* add case to accept non-tokens e.g. variable names DONE */
/* write a test case for 1+1 @119 on piazza DONE */
/* [100] should output '[', T_IntConstant, ']' DONE */
/* Capture all invalid characters */
/* Make sure double quotes are invalid characters */

/*(\/\*([^*]|[\r\n]|[\r]|[\n](\*+([^*\/]|[\r\n])))*\*+\/)|(\/\/.*) {parseComment();} */
